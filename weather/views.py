import requests
from requests import get 
from django.shortcuts import render, redirect
from .models import City
from .forms import CityForm



def index(request):
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=417884e2495672b9d82b5c6296fe8836'
    
    your_city = get('https://ipapi.co/city/').text

    req = requests.get(url.format(your_city)).json()
        
    your_city_weather = {
        'city': your_city,
        'temperature': req['main']['temp'],
        'description' : req['weather'][0]['description'],
        'icon' : req['weather'][0]['icon'],
    }

    err_msg = ''
    message = ''
    message_class = ''

    if request.method == 'POST':
        form = CityForm(request.POST)

        if form.is_valid():
            new_city = form.cleaned_data['name']
            existing_city_count = City.objects.filter(name=new_city).count()
            
            if existing_city_count == 0:
                r = requests.get(url.format(new_city)).json()
                
                if r['cod'] == 200:
                    form.save()
                else:
                    err_msg = 'City does not exist in the world!'
            else:
                err_msg = 'City already exists in the databese!'
    
        if err_msg:
            message = err_msg
            message_class = 'is-danger'
        else:
            message = 'City added successfully!' 
            message_class = 'is-success'

    print(err_msg)

    form = CityForm()

    cities = City.objects.all()

    weather_data = []

    for city in cities:

        r = requests.get(url.format(city)).json()
        
        city_weather = {
            'city': city.name,
            'temperature': r['main']['temp'],
            'description' : r['weather'][0]['description'],
            'icon' : r['weather'][0]['icon'],
        }

        weather_data.append(city_weather)

    

    context = {
        'your_city_weather' : your_city_weather,
        'weather_data' : weather_data, 
        'form' : form,
        'message' : message,
        'message_class' : message_class,
    }
    return render(request, 'weather/weather.html', context)


def delete_city(request, city_name):
    City.objects.get(name=city_name).delete()
    
    return redirect('home')
